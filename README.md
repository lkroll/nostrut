# NoStrut

A Nostr client for Ubuntu Touch

## Features

- Post and reply to notes
- Search for profiles using pubkey (npub)
- Follow and unfollow profiles
- Create account (keypair) and set username
- Import and export private key

## Build Instructions

To build, just run clickable and specify the architecture, clickable will do the rest and bundles the correct libraries into the package.

For arm64 for example, the command looks like this:

```
clickable build --arch arm64
```

## Work of Others

### Libraries

- nostr-sdk python bindings - MIT License: https://github.com/rust-nostr/nostr/tree/master/bindings/nostr-sdk-ffi/bindings-python

### Assets

- Logo by Andrea Nicolini - CC0 License: https://github.com/mbarulli/nostr-logo/
- Placeholder Icon - MIT License: https://github.com/satscoffee/nostr_icons

## License

Copyright (C) 2023  Lennart Kroll

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
