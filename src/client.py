'''
 Copyright (C) 2023  Lennart Kroll

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 nostrut is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import pyotherside
from nostr_sdk import Client, Keys, PublicKey, Filter, Metadata, Event, Timestamp, EventId, EventBuilder, Tag, Contact
from datetime import timedelta
import time
import json

def add_relays():
    client.add_relay("wss://relay.damus.io")
    client.add_relay("wss://offchain.pub")
    client.add_relay("wss://eden.nostr.land")
    client.add_relay("wss://relay.primal.net")
    client.add_relay("wss://nos.lol")

def get_subs(hex_pubkey):
    filter = Filter().author(hex_pubkey).kind(3).limit(1)
    events = client.get_events_of([filter], timedelta(seconds=2))

    subs = []

    if len(events) > 0:
        for tag in events[0].tags():
            subs.append(tag.as_vec()[1])

    return subs

# Get events from hex_pubkeys
def get_events(hex_pubkeys, since_input, until_input):
    filter = Filter().authors(hex_pubkeys).kind(1).since(Timestamp.from_secs(since_input)).until(Timestamp.from_secs(until_input))
    events = client.get_events_of([filter], timedelta(milliseconds=250))

    text_notes = []

    for event in events:
        is_text_note = True

        if len(event.tags()) > 0: # if there are tags check of p and e tags
            for tag in event.tags():
                if tag.as_vec()[0] == "p" or tag.as_vec()[0] == "e": # if event has p or e tag it is a reply or repost
                    is_text_note = False
                    break

        if is_text_note:
            already_added = False

            for text_note in text_notes:
                if text_note.id().to_hex() == event.id().to_hex():
                    already_added = True
                    break

            if not already_added:
                text_notes.append(event)

    text_notes.sort(key=lambda Event: Event.created_at().as_secs(), reverse=True)
    return text_notes

def get_metadata(hex_pubkeys):
    filter = Filter().authors(hex_pubkeys).kind(0)
    events = client.get_events_of([filter], timedelta(milliseconds=200))

    metadata = []

    for event in events:
        already_added = False

        for follower_metadata in metadata:
            if (follower_metadata[0] == event.pubkey().to_hex()):
                already_added = True
                break

        if not already_added:
            follower_metadata = []

            try:
                event_metadata = Metadata.from_json(event.content())
            except:
                continue

            follower_metadata.append(event.pubkey().to_hex())
            follower_metadata.append(event_metadata.get_name())
            follower_metadata.append(event_metadata.get_display_name())
            follower_metadata.append(event_metadata.get_picture())
            follower_metadata.append(event_metadata.get_banner())
            follower_metadata.append(event_metadata.get_website())
            follower_metadata.append(event_metadata.get_about())

            for i, element in enumerate(follower_metadata):
                if follower_metadata[i] == None:
                    follower_metadata[i] = ""

            metadata.append(follower_metadata)

    return metadata

def fetch_events(subs, subs_metadata, since, until):
    events = get_events(subs, since, until) # get events from subs

    for event in events:
        name = ""
        display_name = ""
        picture = ""
        banner = ""
        website = ""
        about = ""

        for i, sub in enumerate(subs_metadata):
            if subs_metadata[i][0] == event.pubkey().to_hex():
                name = subs_metadata[i][1]
                display_name = subs_metadata[i][2]
                picture = subs_metadata[i][3]
                banner = subs_metadata[i][4]
                website = subs_metadata[i][5]
                about = subs_metadata[i][6]
                break

        pyotherside.send("event_fetched", event.id().to_bech32(), event.pubkey().to_bech32(), event.id().to_hex(), event.pubkey().to_hex(), event.content(), event.created_at().as_secs(), event.as_json(), name, display_name, picture, banner, website, about)

    pyotherside.send("events_fetched")

def refetch_events(privatekey, since, until):
    keys = Keys.from_sk_str(privatekey)

    global client
    client = Client(keys)

    add_relays()
    client.connect()

    subs = get_subs(keys.public_key().to_hex())

    if len(subs) > 0:
        pyotherside.send("got_subs", subs)
        subs_metadata = get_metadata(subs) # get metadata from subs (name, picture...)
        pyotherside.send("got_subs_metadata", subs_metadata)
        fetch_events(subs, subs_metadata, since, until)
    else:
        pyotherside.send("no_subs")

def search_profile(npub):
    try:
        public_key = PublicKey.from_bech32(npub)
    except:
        pyotherside.send("no_profile_found")
        return

    profile_metadata = get_metadata([public_key.to_hex()])

    if len(profile_metadata) > 0:
        name = profile_metadata[0][1]
        display_name = profile_metadata[0][2]
        picture = profile_metadata[0][3]
        banner = profile_metadata[0][4]
        website = profile_metadata[0][5]
        about = profile_metadata[0][6]
    else:
        pyotherside.send("no_profile_found")

    pyotherside.send("profile_found", public_key.to_hex(), npub, name, display_name, picture, banner, website, about)

def get_replies(hex_event_id, hex_pubkey, since_input, until_input, depth):

    if since_input is not 0 and until_input is not 0:
        filter = Filter().events([EventId.from_hex(hex_event_id)]).pubkeys([PublicKey.from_hex(hex_pubkey)]).kind(1).since(Timestamp.from_secs(since_input)).until(Timestamp.from_secs(until_input))
    else:
        filter = Filter().events([EventId.from_hex(hex_event_id)]).pubkeys([PublicKey.from_hex(hex_pubkey)]).kind(1)

    events = client.get_events_of([filter], timedelta(milliseconds=200))

    replies = []

    for event in events:
        already_added = False

        for reply in replies:
            if reply.id().to_hex() == event.id().to_hex():
                already_added = True
                break

        if not already_added:
            p_count = 0
            e_count = 0

            for tag in event.tags():
                if tag.as_vec()[0] == "p":
                    p_count += 1
                if tag.as_vec()[0] == "e":
                    e_count += 1

            if (p_count <= depth or e_count <= depth) and p_count == e_count:
                replies.append(event)

    replies.sort(key=lambda Event: Event.created_at().as_secs(), reverse=True)
    return replies

def fetch_replies(hex_event_id, hex_pubkey, since, until, depth):
    replies = get_replies(hex_event_id, hex_pubkey, since, until, depth)

    replies_hex_pubkeys = [] # Save hex pubkeys of replies to get metadata

    for reply in replies:
        replies_hex_pubkeys.append(reply.pubkey().to_hex())

    replies_metadata = get_metadata(replies_hex_pubkeys)

    for reply in replies:
        name = ""
        display_name = ""
        picture = ""
        banner = ""
        website = ""
        about = ""

        for i, replier in enumerate(replies_metadata):
            if (replies_metadata[i][0] == reply.pubkey().to_hex()):
                name = replies_metadata[i][1]
                display_name = replies_metadata[i][2]
                picture = replies_metadata[i][3]
                banner = replies_metadata[i][4]
                website = replies_metadata[i][5]
                about = replies_metadata[i][6]
                break

        pyotherside.send("reply_fetched", reply.id().to_bech32(), reply.pubkey().to_bech32(), reply.id().to_hex(), reply.pubkey().to_hex(), reply.content(), reply.created_at().as_secs(), reply.as_json(), name, display_name, picture, banner, website, about)

    pyotherside.send("replies_fetched")

def publish_note(privatekey, message):
    keys = Keys.from_sk_str(privatekey)
    event = EventBuilder.new_text_note(message, []).to_event(keys)
    client.send_event(event)

def add_reply(privatekey, json, message):
    keys = Keys.from_sk_str(privatekey)
    event = Event.from_json(json)
    # pyotherside.send("print", event.as_json())

    tags = []
    for tag in event.tags():
        if tag.as_vec()[0] == "e":
            tags.append(tag)

    tags.append(Tag.parse(["e", event.id().to_hex()]))

    for tag in event.tags():
        if tag.as_vec()[0] == "p":
            tags.append(tag)

    tags.append(Tag.parse(["p", event.pubkey().to_hex()]))

    reply = EventBuilder.new_text_note(message, tags).to_event(keys)
    # pyotherside.send("print", reply.as_json())
    client.send_event(reply)
    time.sleep(1)

def update_subs(subs, privatekey):
    keys = Keys.from_sk_str(privatekey)

    contacts = []

    for sub in subs:
        contacts.append(Contact(PublicKey.from_hex(sub)))

    event = EventBuilder.set_contact_list(contacts).to_event(keys)
    client.send_event(event)

def update_username(privatekey, username):
    keys = Keys.from_sk_str(privatekey)
    client = Client(keys)

    client.add_relay("wss://relay.damus.io")
    client.add_relay("wss://offchain.pub")
    client.add_relay("wss://eden.nostr.land")
    client.add_relay("wss://relay.primal.net")
    client.add_relay("wss://nos.lol")

    client.connect()

    metadata = Metadata().set_name(username)
    client.set_metadata(metadata)

    client.disconnect()
    pyotherside.send("username_updated")

def fetch_posts(hex_pubkey, since, until):
    events = get_events([hex_pubkey], since, until)

    for event in events:
        pyotherside.send("post_fetched", event.id().to_bech32(), event.id().to_hex(), event.content(), event.created_at().as_secs(), event.as_json())

    pyotherside.send("posts_fetched")

def get_reactions(hex_event_id, hex_pubkey):
    filter = Filter().event(EventId.from_hex(hex_event_id)).pubkey(PublicKey.from_hex(hex_pubkey)).kind(7)
    events = client.get_events_of([filter], timedelta(milliseconds=200))

    return events

def fetch_reactions(hex_event_id, hex_pubkey, type):
    reactions = get_reactions(hex_event_id, hex_pubkey)

    likers = []

    for reaction in reactions:
        already_added = False

        for liker in likers:
            if liker == reaction.pubkey().to_hex():
                already_added = True
                break

        if not already_added:
            if reaction.content() is not "-":
                likers.append(reaction.pubkey().to_hex())

    if type == "events":
        pyotherside.send("got_event_reactions", hex_event_id, likers)
    if type == "replies":
        pyotherside.send("got_reply_reactions", hex_event_id, likers)
    if type == "posts":
        pyotherside.send("got_post_reactions", hex_event_id, likers)

def add_reaction(privatekey, hex_event_id, hex_pubkey):
    keys = Keys.from_sk_str(privatekey)
    reaction = EventBuilder.new_reaction(EventId.from_hex(hex_event_id), PublicKey.from_hex(hex_pubkey), "+").to_event(keys)
    # pyotherside.send("print", reaction.as_json())
    client.send_event(reaction)

def fetch_reply_count(hex_event_id, hex_pubkey, depth, type):
    replies = get_replies(hex_event_id, hex_pubkey, 0, 0, depth)

    if type == "events":
        pyotherside.send("got_event_replies", hex_event_id, len(replies))
    if type == "replies":
        pyotherside.send("got_reply_replies", hex_event_id, len(replies))
    if type == "posts":
        pyotherside.send("got_post_replies", hex_event_id, len(replies))

# TEMP: limit 100 until handle_notifications / subscriptions are implemented in nostr-sdk python bindings
# Getting more followers slows down / breaks loading of notes on profiles
def get_followers(hex_pubkey):
    filter = Filter().pubkey(PublicKey.from_hex(hex_pubkey)).kind(3).limit(100)
    events = client.get_events_of([filter], timedelta(seconds=1))

    followers = []

    for event in events:
        if event.pubkey().to_hex() not in followers:
            followers.append(event.pubkey().to_hex())

    # for follower in followers:
    #     pyotherside.send("print", follower)

    return followers

def update_metadata(username, display_name, picture, banner, website, about):
    metadata_json = {}

    if username:
        metadata_json["name"] = username
    if display_name:
        metadata_json["display_name"] = display_name
    if picture:
        metadata_json["picture"] = picture
    if banner:
        metadata_json["banner"] = banner
    if website:
        metadata_json["website"] = website
    if about:
        metadata_json["about"] = about

    metadata = Metadata().from_json(json.dumps(metadata_json))
    # pyotherside.send("print", metadata.as_json())
    client.set_metadata(metadata)

def start(privatekey, since, until):
    keys = Keys.from_sk_str(privatekey)

    global client
    client = Client(keys)

    add_relays()
    client.connect()

    subs = get_subs(keys.public_key().to_hex())

    if len(subs) > 0:
        pyotherside.send("got_subs", subs)
        subs_metadata = get_metadata(subs) # get metadata from subs (name, picture...)
        pyotherside.send("got_subs_metadata", subs_metadata)
        fetch_events(subs, subs_metadata, since, until)
    else:
        pyotherside.send("no_subs")

def close():
    client.disconnect()
