function getCombinedName(name, display_name, npub){
    let combinedName = "";

    if (name == "" && display_name == "") {
        combinedName = npub;
    }
    else if (display_name == "") {
        combinedName = "@" + name;
    }
    else if (name == "") {
        combinedName = display_name;
    }
    else {
        combinedName = display_name + " (@" + name + ")";
    }

    return combinedName;
}

function getCombinedName2(name, display_name){
    let combinedName = "";

    if (name == "" && display_name == "") {
        combinedName = "";
    }
    else if (display_name == "") {
        combinedName = "@" + name;
    }
    else if (name == "") {
        combinedName = display_name;
    }
    else {
        combinedName = display_name + " (@" + name + ")";
    }

    return combinedName;
}

function getDate(date){
    let now = Math.floor(Date.now() / 1000);
    let diff_mins = Math.floor((now - date) / 60);
    let diff_hours = Math.floor(diff_mins / 60);
    let diff_days = Math.floor(diff_hours / 24);
    let diff_weeks = Math.floor(diff_days / 7);

    if (diff_mins <= 1) {
        date = i18n.tr("Just now");
    }
    else if (diff_mins > 1 && diff_hours < 1) {
        date = diff_mins + i18n.tr(" mins ago");
    }
    else if (diff_hours >= 1 && diff_days < 1) {
        date = diff_hours + i18n.tr("h ago");
    }
    else if (diff_days == 1) {
        date = i18n.tr("1 day ago");
    }
    else if (diff_days > 1 && diff_weeks < 1) {
        date = diff_days + i18n.tr(" days ago");
    }
    else if (diff_weeks == 1) {
        date = i18n.tr("1 week ago");
    }
    else if (diff_weeks > 1 && diff_weeks < 4) {
        date = diff_weeks + i18n.tr(" weeks ago");
    }
    else {
        let date_obj = new Date(date * 1000);
        date = date_obj.toLocaleString(Qt.locale(), Qt.DefaultLocale, "dd MMM yyyy hh:mm:ss");
    }

    return date;
}

function getImgURLs(content){
    let imgURLs = []
    let imageUrlRegExp = /(https?:\/\/\S+\.(?:jpg|jpeg|png|webp|gif))/gi;
    let match = imageUrlRegExp.exec(content);

    while (match) {
        imgURLs.push(match[0]);
        match = imageUrlRegExp.exec(content);
    }

    return imgURLs;
}

function createClickableLinks(content){
    let pattern = /(https?:\/\/[^\s]+)/g;
    return content.replace(pattern, "<a href='$1' style='color: DodgerBlue; text-decoration: none;'>$1</a>");
}
