import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: aboutPage

    Component.onCompleted: {
        aboutPageListModel.append({
            aboutIcon: "text-xml-symbolic",
            aboutText: i18n.tr("Source Code"),
            aboutLink: "https://gitlab.com/lkroll/nostrut"
        });

        aboutPageListModel.append({
            aboutIcon: "like",
            aboutText: i18n.tr("Donate some Sats"),
            aboutLink: "https://getalby.com/p/scribdo"
        });

        aboutPageListModel.append({
            aboutIcon: "stock_image",
            aboutText: i18n.tr("Logo Source"),
            aboutLink: "https://github.com/mbarulli/nostr-logo/"
        });

        aboutPageListModel.append({
            aboutIcon: "flash-on",
            aboutText: i18n.tr("Donate to Andrea Nicolini"),
            aboutLink: "https://getalby.com/p/bembureda"
        });
    }

    header: PageHeader {
        id: aboutHeader
        title: i18n.tr("About")
    }

    ScrollView {
        id: aboutPageScrollView
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentItem: Rectangle {
            color: theme.palette.normal.background
            width: aboutPageScrollView.width
            height: aboutPageColumn.implicitHeight + units.gu(8)

            Column {
                id: aboutPageColumn
                spacing: units.gu(4)
                anchors {
                    fill: parent
                    margins: units.gu(4)
                }
                Column {
                    spacing: units.gu(2)
                    anchors.horizontalCenter: parent.horizontalCenter
                    Image {
                        id: logo
                        source: "../../assets/logo.svg"
                        fillMode: Image.PreserveAspectFit
                        width: units.gu(20)
                        height: units.gu(20)
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Label {
                        id: description
                        text: i18n.tr("A Nostr client for Ubuntu Touch")
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Label {
                        id: imgLicense
                        text: i18n.tr("Logo licensed under CC0 by Andrea Nicolini")
                        anchors.horizontalCenter: parent.horizontalCenter
                        textSize: Label.Small
                    }
                }
                ListView {
                    id: aboutPageListView
                    height: units.gu(32)
                    width: units.gu(30)

                    anchors.horizontalCenter: parent.horizontalCenter

                    model: aboutPageListModel

                    delegate: ListItem {
                        width: parent.width
                        height: units.gu(8)
                        divider.visible: false

                        onClicked: Qt.openUrlExternally(aboutLink)

                        Row {
                            spacing: units.gu(2)
                            anchors {
                                verticalCenter: parent.verticalCenter
                                horizontalCenter: parent.horizontalCenter
                            }
                            Icon {
                                height: units.gu(3)
                                name: aboutIcon
                            }
                            Label {
                                text: aboutText
                                textSize: Label.Medium
                            }
                        }
                    }
                }

                ListModel {
                    id: aboutPageListModel
                }
            }
        }
    }
}
