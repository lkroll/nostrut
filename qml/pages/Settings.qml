import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: settingsPage

    Component.onCompleted: {
        settingsListModel.append({
            settingIcon: "account",
            settingName: i18n.tr("Profile Settings"),
            settingPage: "ProfileSettings.qml"
        });

        settingsListModel.append({
            settingIcon: "stock_key",
            settingName: i18n.tr("Manage Keys"),
            settingPage: "KeySettings.qml"
        });

        settingsListModel.append({
            settingIcon: "stock_website",
            settingName: i18n.tr("Configure Relays"),
            settingPage: "RelaySettings.qml"
        });
    }

    header: PageHeader {
        id: settingsHeader
        title: i18n.tr("Settings")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "info"
                    onTriggered: mainStack.push(Qt.resolvedUrl("About.qml"))
                }
            ]
        }
    }

    ListView {
        id: settingsListView

        anchors {
            top: settingsHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: settingsListModel

        delegate: ListItem {
            width: parent.width
            height: units.gu(9)

            onClicked: {
                if (settingPage == "ProfileSettings.qml") {
                    let profile_metadata = python.call_sync("client.get_metadata", [[keySettings.publicKeyHex]]);

                    let name = profile_metadata[0][1];
                    let display_name = profile_metadata[0][2];
                    let picture = profile_metadata[0][3];
                    let banner = profile_metadata[0][4];
                    let website = profile_metadata[0][5];
                    let about = profile_metadata[0][6];

                    mainStack.push(Qt.resolvedUrl(settingPage), {
                        "name": name,
                        "display_name": display_name,
                        "picture": picture,
                        "banner": banner,
                        "banner": banner,
                        "website": website,
                        "about": about
                    });
                }
                else {
                    mainStack.push(Qt.resolvedUrl(settingPage));
                }
            }

            Row {
                spacing: units.gu(2)
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: units.gu(2)
                }
                Icon {
                    height: units.gu(3)
                    name: settingIcon
                }
                Label {
                    text: settingName
                    textSize: Label.Medium
                }
            }
        }
    }

    ListModel {
        id: settingsListModel
    }
}
