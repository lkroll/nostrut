import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import "../components"
import "../js/functions.js" as Functions

Page {
    id: searchPage

    property bool searching: false

    Component.onCompleted: searchField.focus = true

    header: PageHeader {
        id: header

        trailingActionBar {
            actions: [
                Action {
                    visible: false
                    iconName: "filters"
                    // onTriggered: mainStack.push(Qt.resolvedUrl("About.qml"))
                }
            ]
        }

        TextField {
            id: searchField
            anchors {
                fill: parent
                leftMargin: units.gu(6)
                rightMargin: units.gu(6)
                topMargin: units.gu(1)
                bottomMargin: units.gu(1)
            }

            placeholderText: i18n.tr("Search for npub")
            onAccepted: {
                searching = true;
                profilesListModel.clear();
                python.call("client.search_profile", [searchField.text], function(){
                    searching = false;
                });
            }
        }
    }

    ProgressBar {
        id: progressBar
        indeterminate: true
        visible: searching
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    ListView {
        id: profilesListView

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: profilesListModel

        delegate: ListItem {
            id: listItem
            width: parent.width
            height: units.gu(9)

            onClicked: {
                mainStack.push(Qt.resolvedUrl("ProfilePage.qml"), {
                    "hex_pubkey": hex_pubkey,
                    "npub": npub,
                    "name": name,
                    "display_name": display_name,
                    "picture": picture,
                    "banner": banner,
                    "banner": banner,
                    "website": website,
                    "about": about
                });
                searchField.text = "";
            }

            Row {
                spacing: units.gu(2)
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: units.gu(2)
                }
                Image {
                    id: profilePic
                    source: picture
                    height: units.gu(7)
                    fillMode: Image.PreserveAspectFit
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: combinedName
                    textSize: Label.Medium
                    wrapMode: Label.WrapAnywhere
                    width: listItem.width - profilePic.width - units.gu(8)
                }
            }
        }
    }

    ListModel {
        id: profilesListModel
    }

    PopupRectangle {
        id: noProfilePopup
        anchors {
            top: header.bottom
            horizontalCenter: parent.horizontalCenter
            topMargin: units.gu(2)
        }
    }

    Timer {
        id: hideNoProfilePopupTimer
        interval: 3000
        onTriggered: noProfilePopup.visible = false;
    }

    Python {
        Component.onCompleted: {
            setHandler("profile_found", function(hex_pubkey, npub, name, display_name, picture, banner, website, about){
                let combinedName = Functions.getCombinedName(name, display_name, npub);

                profilesListModel.append({
                    "hex_pubkey": hex_pubkey,
                    "npub": npub,
                    "name": name,
                    "display_name": display_name,
                    "combinedName": combinedName,
                    "picture": picture,
                    "banner": banner,
                    "website": website,
                    "about": about
                });
            });

            setHandler("no_profile_found", function(){
                noProfilePopup.content = "No Profile found or not a valid Key";
                noProfilePopup.visible = true;
                hideNoProfilePopupTimer.start();
            })
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
