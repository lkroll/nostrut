import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Page {
    id: imagePage

    property string imgURL: ""
    property string imageTitle: i18n.tr("Image")

    header: PageHeader {
        id: imagePageHeader
        title: imageTitle
        subtitle: imgURL

        trailingActionBar {
            actions: [
                Action {
                    iconName: "external-link"
                    onTriggered: Qt.openUrlExternally(imgURL)
                }
            ]
        }
    }

    Image {
        anchors {
            top: imagePageHeader.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        source: imgURL
        fillMode: Image.PreserveAspectFit
    }
}
