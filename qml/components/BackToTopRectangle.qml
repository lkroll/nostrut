import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Rectangle {
    id: backToTopRectangle
    z: 1
    visible: goUp
    anchors {
        bottom: parent.bottom
        bottomMargin: units.gu(4)
        right: parent.right
        rightMargin: units.gu(4)
    }
    opacity: 0.85
    width: units.gu(6)
    height: width
    color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.silk : LomiriColors.slate
    radius: units.gu(1)
    Icon {
        name: "go-up"
        width: parent.width - units.gu(2)
        height: width
        color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.inkstone : LomiriColors.porcelain
        anchors.centerIn: parent
    }
}
