import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Component {
    id : eventInfoPopupDialog
    Dialog {
        id: eventInfoPopup
        Column {
            spacing: units.gu(2)
            EventInfoPopupDialogColumn {
                iconName: "info"
                heading: "Event ID"
                content: event_id
            }
            EventInfoPopupDialogColumn {
                iconName: "stock_key"
                heading: "Public Key"
                content: npub
            }
        }
        Button {
            text: i18n.tr("Close")
            onClicked: PopupUtils.close(eventInfoPopup)
        }
    }
}
