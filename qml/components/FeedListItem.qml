import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

ListItem {
    property bool isParent: false
    property bool replying: false
    property bool sending: false
    property bool profileClickable: true
    property bool likeInitiated: false

    width: parent.width
    height: noteColumn.implicitHeight + units.gu(5)
    divider.visible: false

    Rectangle {
        id: noteRectangle

        border {
            width: isParent ? units.gu(0.25) : 0
            color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.silk : LomiriColors.slate
        }
        height: noteColumn.implicitHeight + units.gu(5)
        color: theme.name == "Lomiri.Components.Themes.Ambiance" ? LomiriColors.porcelain : LomiriColors.inkstone
        radius: units.gu(2)
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }
        FeedListItemColumn {
            id: noteColumn
            anchors {
                fill: parent
                margins: units.gu(2)
            }
        }
    }

    trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "info"
                onTriggered: PopupUtils.open(eventInfoPopupDialog)
            },
            Action {
                iconName: "edit-copy"
                onTriggered: Clipboard.push(content)
            },
            Action {
                iconName: "external-link"
                onTriggered: Qt.openUrlExternally("https://njump.me/" + event_id)
            },
            Action {
                iconName: "mail-reply"
                onTriggered: replying = true
            }
        ]
    }

    EventInfoPopupDialog {
        id : eventInfoPopupDialog
    }
}
