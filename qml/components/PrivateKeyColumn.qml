import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

Column {
    id: privateKeyColumn
    visible: keySettings.keySet
    spacing: units.gu(1)
    Label {
        text: "Private Key:"
    }
    Row {
        spacing: units.gu(2)
        Label {
            id: privateKeyLabel
            text: privateKeyVisible ? keySettings.privateKey : "*".repeat(keySettings.privateKey.length)
            width: keySettingsColumn.width - copyPrivateKeyIcon.width - viewPrivateKeyIcon.width - units.gu(4)
            wrapMode: Label.WrapAnywhere
        }
        Icon {
            id: viewPrivateKeyIcon
            name: keySettingsPage.privateKeyVisible ? "view-off" : "view-on"
            width: units.gu(3)

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (keySettingsPage.privateKeyVisible) {
                        keySettingsPage.privateKeyVisible = false;
                    }
                    else {
                        keySettingsPage.privateKeyVisible = true;
                    }
                }
            }
        }
        Icon {
            id: copyPrivateKeyIcon
            name: "edit-copy"
            width: units.gu(3)

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Clipboard.push(keySettings.privateKey);
                    clipboardPopup.content = "Private Key copied";
                    clipboardPopup.visible = true;
                    hideClipboardPopupTimer.start()
                }
            }
        }
    }
}
